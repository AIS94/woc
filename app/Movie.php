<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title', 'release_date', 'running_time', 'genre', 'cast', 'director', 'production', 'language', 'age', 'poster', 'yt_link', 'price','rating'];
}
