<?php

namespace App\Http\Controllers\Api;

use App\Movie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MovieController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('movie');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $movie = Movie::create([
            'title'        => request('title'),
            'release_date' => request('release_date'),
            'running_time' => request('running_time'),
            'genre'        => request('genre'),
            'cast'         => request('cast'),
            'director'     => request('director'),
            'production'   => request('production'),
            'language'     => request('language'),
            'age'          => request('age'),
            'poster'       => request('poster'),
            'yt_link'      => request('yt_link'),
            'price'        => request('price'),
            'rating'       => request('rating')
        ]);

        return response()->json([
            'movie'    => $movie,
            'message' => 'Movie added successfully!'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

            $movie->title        = request('title');
            $movie->release_date = request('release_date');
            $movie->running_time = request('running_time');
            $movie->genre        = request('genre');
            $movie->cast         = request('cast');
            $movie->director     = request('director');
            $movie->production   = request('production');
            $movie->language     = request('language');
            $movie->age          = request('age');
            $movie->poster       = request('poster');
            $movie->yt_link      = request('yt_link');
            $movie->price        = request('price');
            $movie->rating       = request('rating');
            
            $movie->save();

        return response()->json([
            'message' => 'Movie updated successfully!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie->delete();

        return response()->json([
            'message' => 'Movie deleted successfully!'
        ], 200);
    }

    public function getMovies(){
        $movies = Movie::all();

        return response()->json([
            'movies' => $movies,
        ], 200);
    }
}
