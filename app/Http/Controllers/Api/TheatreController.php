<?php

namespace App\Http\Controllers\Api;

use App\Theatre;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TheatreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('theatre');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $theatre = Theatre::create([
            'name'          => request('name'),
            'capacity'      => request('capacity'),
            'hours'         => request('hours'),
        ]);

        return response()->json([
            'theatre'    => $theatre,
            'message' => 'Theatre added successfully!'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function show(Theatre $theatre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function edit(Theatre $theatre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theatre  $theatre
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Theatre $theatre)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

            $theatre->name       = request('name');
            $theatre->capacity   = request('capacity');
            $theatre->hours      = request('hours');

            $theatre->save();

        return response()->json([
            'message' => 'Theatre updated successfully!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Theatre  $theatre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theatre $theatre)
    {
        $theatre->delete();

        return response()->json([
            'message' => 'Theatre deleted successfully!'
        ], 200);
    }

    public function getTheatres(){
        $theatres = Theatre::all();

        return response()->json([
            'theatres' => $theatres,
        ], 200);
    }
}
