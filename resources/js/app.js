require('./bootstrap');
require('angular');
require('angular-route');
import swal from 'sweetalert';

var app = angular.module('LaravelCRUD', ['ngRoute'], ['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
        console.log('WOC initialization done.');
    }]);

app.run(function($rootScope) {
    $rootScope.isAuth = !!window.user;

    if($rootScope.isAuth)
        $rootScope.user = JSON.parse(window.user);
});

app.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            controller:'HomeController',
            templateUrl:'home'
        }).when('/movies', {
            controller:'MovieController',
            templateUrl:'api/movie',
            resolve: {
                app: function($q, $rootScope, $location) {
                    var defer = $q.defer();
                    if (!$rootScope.isAuth) {
                        $location.path('/');
                    };
                    defer.resolve();
                    return defer.promise;
                }
            }
        }).when('/theatres', {
            controller:'TheatreController',
            templateUrl:'api/theatre',
            resolve: {
                app: function($q, $rootScope, $location) {
                    var defer = $q.defer();
                    if (!$rootScope.isAuth) {
                        $location.path('/');
                    };
                    defer.resolve();
                    return defer.promise;
                }
            }
        }).otherwise({
            redirectTo: '/'
        });

});

app.controller('MovieController', ['$scope', '$http', function ($scope, $http) {
    $scope.ctrl = 'MovieController';

    // List movies
    $scope.loadMovies = function () {
        $http.get('api/movie/getMovies')
            .then(function success(response) {
                $scope.movies = response.data.movies;
                console.log('getMovies', $scope.movies);
            });
    };
    $scope.loadMovies();

    $scope.errors = [];

    $scope.movie = {
        title: '',
        release_date: '',
        running_time: '',
        genre: '',
        cast: '',
        director: '',
        production: '',
        language: '',
        age: '',
        poster: '',
        yt_link: '',
        price: '',
        rating: ''
    };
    $scope.initMovie = function () {
        $scope.poster = ''; //todo
        $scope.resetForm();
        $("#add_new_movie").modal('show');
    };

    // Add new Movie
    $scope.addMovie = function () {
        $http.post('/api/movie', {
            title: $scope.movie.title,
            release_date: $scope.movie.release_date,
            running_time: $scope.movie.running_time,
            genre: $scope.movie.genre,
            cast: $scope.movie.cast,
            director: $scope.movie.director,
            production: $scope.movie.production,
            language: $scope.movie.language,
            age: $scope.movie.age,
            poster: $scope.poster,
            yt_link: $scope.movie.yt_link,
            price: $scope.movie.price,
            rating: $scope.rating

        }).then(function success(response) {
            $scope.resetForm();
            $scope.movies.push(response.data.movie);
            $("#add_new_movie").modal('hide');
            swal("Added!", response.data.message, "success");
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };

    $scope.searchMovie = function(){
        $http.get('http://www.omdbapi.com/?apikey=b4c788cc&t=' + $scope.movie.title)
            .then(function(data){
                console.log(data);
                $scope.poster = data.data.Poster;
                $scope.rating = data.data.imdbRating;
            });
    }

    $scope.searchMovie2 = function(){
        $http.get('http://www.omdbapi.com/?apikey=b4c788cc&t=' + $scope.edit_movie.title)
            .then(function(data){
                $scope.edit_movie.poster = data.data.Poster;
                $scope.edit_movie.rating = data.data.imdbRating;
            });
    }

    $scope.recordErrors = function (error) {
        $scope.errors = [];
        if (error.data.errors.title) {
            $scope.errors.push(error.data.errors.title[0]);
        }
    };

    $scope.resetForm = function () {
        $scope.movie.title = '';
        $scope.movie.release_date = '';
        $scope.movie.running_time = '';
        $scope.movie.genre = '';
        $scope.movie.cast = '';
        $scope.movie.director = '';
        $scope.movie.production = '';
        $scope.movie.language = '';
        $scope.movie.age = '';
        $scope.poster = '';
        $scope.movie.yt_link = '';
        $scope.movie.price = '';
        $scope.rating = '';

        $scope.errors = [];
    };

    $scope.edit_movie = {};
    // initialize update action
    $scope.initEdit = function (index) {
        $scope.errors = [];
        $scope.edit_movie = $scope.movies[index];
        $("#edit_movie").modal('show');
    };

    // update the given movie
    $scope.updateMovie = function () {
        $http.patch('/api/movie/' + $scope.edit_movie.id, {
            title: $scope.edit_movie.title,
            release_date: $scope.edit_movie.release_date,
            running_time: $scope.edit_movie.running_time,
            genre: $scope.edit_movie.genre,
            cast: $scope.edit_movie.cast,
            director: $scope.edit_movie.director,
            production: $scope.edit_movie.production,
            language: $scope.edit_movie.language,
            age: $scope.edit_movie.age,
            poster: $scope.edit_movie.poster,
            yt_link: $scope.edit_movie.yt_link,
            price: $scope.edit_movie.price,
            rating: $scope.edit_movie.rating

        }).then(function success(response) {
            $scope.errors = [];
            $("#edit_movie").modal('hide');
            swal("Updated!", response.data.message, "success");
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };

    // delete the given movie
    $scope.deleteMovie = function (index) {
        
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this movie?",
            icon: "warning",
            dangerMode: true,
          })
          .then(function(willDelete){
            if (willDelete) {
                $http.delete('/api/movie/' + $scope.movies[index].id)
              .then(function success(response){
                $scope.movies.splice(index, 1);
                swal("Deleted!", response.data.message, "success");
              });
            }
          })

    };
}]);

app.controller('TheatreController', ['$scope', '$http', function ($scope, $http) {
    $scope.ctrl = 'TheatreController';

    // List movies
    $scope.loadTheatres = function () {
        $http.get('api/theatre/getTheatres')
            .then(function success(response) {
                $scope.theatres = response.data.theatres;
                console.log('getTheatres', $scope.theatres);
            });
    };
    $scope.loadTheatres();

    $scope.errors = [];

    $scope.theatre = {
        name: '',
        capacity: '',
        hours: '',
    };
    $scope.initTheatre = function () {
        $scope.resetForm();
        $("#add_new_theatre").modal('show');
    };

    // Add new Movie
    $scope.addTheatre = function () {
        $http.post('/api/theatre', {
            name: $scope.theatre.name,
            capacity: $scope.theatre.capacity,
            hours: $scope.theatre.hours,
        }).then(function success(response) {
            $scope.resetForm();
            $scope.theatres.push(response.data.theatre);
            $("#add_new_theatre").modal('hide');
            swal("Added!", response.data.message, "success");
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };

    $scope.recordErrors = function (error) {
        $scope.errors = [];
        if (error.data.errors.title) {
            $scope.errors.push(error.data.errors.title[0]);
        }
    };

    $scope.resetForm = function () {
        $scope.theatre.name = '';
        $scope.theatre.capacity = '';
        $scope.theatre.hours = '';
        $scope.errors = [];
    };

    $scope.edit_theatre = {};
    // initialize update action
    $scope.initEdit = function (index) {
        $scope.errors = [];
        $scope.edit_theatre = $scope.theatres[index];
        $("#edit_theatre").modal('show');
    };

    // update the given movie
    $scope.updateTheatre = function () {
        $http.patch('/api/theatre/' + $scope.edit_theatre.id, {
            name: $scope.edit_theatre.name,
            capacity: $scope.edit_theatre.capacity,
            hours: $scope.edit_theatre.hours,
        }).then(function success(response) {
            $scope.errors = [];
            $("#edit_theatre").modal('hide');
            swal("Updated!", response.data.message, "success");
        }, function error(error) {
            $scope.recordErrors(error);
        });
    };

    // delete the given movie
    $scope.deleteTheatre = function (index) {
        
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to delete this theatre?",
            icon: "warning",
            dangerMode: true,
          })
          .then(function(willDelete){
            if (willDelete) {
            $http.delete('/api/theatre/' + $scope.theatres[index].id)
              .then(function success(response){
                $scope.theatres.splice(index, 1);
                swal("Deleted!", response.data.message, "success");
              });
            }
          })

    };
}]);

app.controller('HomeController', ['$scope', '$http', function ($scope, $http) {
    $scope.ctrl = 'HomeController';

    // List movies
    $scope.loadMovies = function () {
        $http.get('api/movie/getMovies')
            .then(function success(response) {
                $scope.movies = response.data.movies;
                console.log('getMovies', $scope.movies);
            });
    };
    $scope.loadMovies();


}]);