<div class="container my-5">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" ng-if="movies.length > 0">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" ng-repeat="movie in movies track by $index" data-slide-to="$index" ng-class="{active: $index == 0}"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item" ng-repeat="movie in movies track by $index" ng-class="{active: $index == 0}">
        <img class="d-block w-100" ng-src="@{{ movie.poster }}" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
            <h2>@{{movie.title}}</h2>
        </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" target="_self" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" target="_self" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>
</div>

<div class="container movies-wrapper my-5" ng-if="movies.length > 0" ng-cloak>
    <div class="row flex-column-reverse flex-md-row">
        <div class="col-md-4" ng-repeat="movie in movies">
            <div class="card">
                <div class="card-header">
                    <img class="card-img" ng-src="@{{ movie.poster }}" alt="@{{ movie.title }}">
                </div>
                <div class="card-body">
                    <h1 class="card-title">@{{ movie.title }}</h1>
                    <div class="container">
                        <div class="row">
                            <div class="col-4 metadata">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <p>@{{movie.rating}}/10</p>
                            </div>
                            <div class="col-4 metadata">@{{ movie.genre }}</div>
                            <div class="col-4 metadata">@{{ movie.production }}</div>
                        </div>
                        <div class="row">
                            <div class="col-6 metadata">@{{ movie.running_time + ' min' }}</div>
                            <div class="col-6 metadata">@{{movie.price + ' RON'}}</div>
                        </div>
                    </div>
                    <a class="trailer-preview" href="@{{ movie.yt_link }}" target="new">
                        <i class="fa fa-play" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
