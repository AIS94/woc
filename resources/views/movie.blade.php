<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button class="btn btn-primary btn-xs pull-left my-4" ng-click="initMovie()">Add Movie</button>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered table-striped" ng-if="movies.length > 0">
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Release Date</th>
                            <th>Running Time</th>
                            <th>Genre</th>
                            <th>Cast</th>
                            <th>Director</th>
                            <th>Production</th>
                            <th>Language</th>
                            <th>Age</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        <tr ng-repeat="(index, movie) in movies">
                            <td>
                                @{{ index + 1 }}
                            </td>
                            <td>@{{ movie.title }}</td>
                            <td>@{{ movie.release_date }}</td>
                            <td>@{{ movie.running_time + ' min' }}</td>
                            <td>@{{ movie.genre }}</td>
                            <td>@{{ movie.cast }}</td>
                            <td>@{{ movie.director }}</td>
                            <td>@{{ movie.production }}</td>
                            <td>@{{ movie.language }}</td>
                            <td>@{{ movie.age }}</td>
                            <td>@{{ movie.price }}</td>
                            <td>
                                <button class="btn btn-success btn-xs" ng-click="initEdit(index)">Edit</button>
                                <button class="btn btn-danger btn-xs" ng-click="deleteMovie(index)" >Delete</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="add_new_movie">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Movie</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>
                    <img ng-if="poster" ng-src="@{{poster}}" style="height: 200px;margin: 0 auto;display: block;"/>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text"
                               name="title"
                               class="form-control"
                               ng-model="movie.title"
                               ng-change="searchMovie()"
                               ng-model-options="{ updateOn: 'default blur', debounce: 300 }">
                    </div>
                    <div class="form-group">
                        <label for="release_date">Release Date</label>
                        <input type="text" name="release_date" class="form-control" ng-model="movie.release_date">
                    </div>
                    <div class="form-group">
                        <label for="running_time">Running Time</label>
                        <input type="text" name="running_time" class="form-control" ng-model="movie.running_time">
                    </div>
                    <div class="form-group">
                        <label for="genre">Genre</label>
                        <input type="text" name="genre" class="form-control" ng-model="movie.genre">
                    </div>
                    <div class="form-group">
                        <label for="cast">Cast</label>
                        <input type="text" name="cast" class="form-control" ng-model="movie.cast">
                    </div>
                    <div class="form-group">
                        <label for="director">Director</label>
                        <input type="text" name="director" class="form-control" ng-model="movie.director">
                    </div>
                    <div class="form-group">
                        <label for="production">Production</label>
                        <input type="text" name="production" class="form-control" ng-model="movie.production">
                    </div>
                    <div class="form-group">
                        <label for="language">Language</label>
                        <input type="text" name="language" class="form-control" ng-model="movie.language">
                    </div>
                    <div class="form-group">
                        <label for="age">Age</label>
                        <input type="text" name="age" class="form-control" ng-model="movie.age">
                    </div>
                    <div class="form-group">
                        <label for="yt_link">Youtube Link</label>
                        <input type="text" name="yt_link" class="form-control" ng-model="movie.yt_link">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" class="form-control" ng-model="movie.price">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="addMovie()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="edit_movie">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Movie</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>

                    <img ng-if="edit_movie.poster" ng-src="@{{edit_movie.poster}}" style="height: 200px;margin: 0 auto;display: block;"/>

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text"
                               name="title"
                               class="form-control"
                               ng-model="edit_movie.title"
                               ng-change="searchMovie2()"
                               ng-model-options="{ updateOn: 'default blur', debounce: 300 }">
                    </div>
                    <div class="form-group">
                        <label for="release_date">Release Date</label>
                        <input type="text" name="release_date" class="form-control" ng-model="edit_movie.release_date">
                    </div>
                    <div class="form-group">
                        <label for="running_time">Running Time</label>
                        <input type="text" name="running_time" class="form-control" ng-model="edit_movie.running_time">
                    </div>
                    <div class="form-group">
                        <label for="genre">Genre</label>
                        <input type="text" name="genre" class="form-control" ng-model="edit_movie.genre">
                    </div>
                    <div class="form-group">
                        <label for="cast">Cast</label>
                        <input type="text" name="cast" class="form-control" ng-model="edit_movie.cast">
                    </div>
                    <div class="form-group">
                        <label for="director">Director</label>
                        <input type="text" name="director" class="form-control" ng-model="edit_movie.director">
                    </div>
                    <div class="form-group">
                        <label for="production">Production</label>
                        <input type="text" name="production" class="form-control" ng-model="edit_movie.production">
                    </div>
                    <div class="form-group">
                        <label for="language">Language</label>
                        <input type="text" name="language" class="form-control" ng-model="edit_movie.language">
                    </div>
                    <div class="form-group">
                        <label for="age">Age</label>
                        <input type="text" name="age" class="form-control" ng-model="edit_movie.age">
                    </div>
                    <div class="form-group">
                        <label for="yt_link">Youtube Link</label>
                        <input type="text" name="yt_link" class="form-control" ng-model="edit_movie.yt_link">
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" class="form-control" ng-model="edit_movie.price">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="updateMovie()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>