<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <button class="btn btn-primary btn-xs pull-left my-4" ng-click="initTheatre()">Add Theatre</button>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered table-striped" ng-if="theatres.length > 0">
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Capacity</th>
                            <th>Hours</th>
                            <th>Action</th>
                        </tr>
                        <tr ng-repeat="(index, theatre) in theatres">
                            <td>
                                @{{ index + 1 }}
                            </td>
                            <td>@{{ theatre.name }}</td>
                            <td>@{{ theatre.capacity }}</td>
                            <td>@{{ theatre.hours }}</td>
                            <td>
                                <button class="btn btn-success btn-xs" ng-click="initEdit(index)">Edit</button>
                                <button class="btn btn-danger btn-xs" ng-click="deleteTheatre(index)">Delete</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="add_new_theatre">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Theatre</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" ng-model="theatre.name">
                    </div>
                    <div class="form-group">
                        <label for="capacity">Capacity</label>
                        <input type="text" name="capacity" class="form-control" ng-model="theatre.capacity">
                    </div>
                    <div class="form-group">
                        <label for="hours">Hours</label>
                        <input type="text" name="hours" class="form-control" ng-model="theatre.hours">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="addTheatre()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="edit_theatre">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Theatre</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger" ng-if="errors.length > 0">
                        <ul>
                            <li ng-repeat="error in errors">@{{ error }}</li>
                        </ul>
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" ng-model="edit_theatre.name">
                    </div>
                    <div class="form-group">
                        <label for="capacity">Capacity</label>
                        <input type="text" name="capacity" class="form-control" ng-model="edit_theatre.capacity">
                    </div>
                    <div class="form-group">
                        <label for="hours">Hours</label>
                        <input type="text" name="hours" class="form-control" ng-model="edit_theatre.hours">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="updateTheatre()">Submit</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>