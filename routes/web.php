<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
    return view('layouts.app-ng');
});

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::group(['prefix' => 'api','namespace'=>'Api'], function(){
    Route::get('/movie/getMovies', 'MovieController@getMovies');
    Route::resource('/movie', 'MovieController');
});

Route::group(['prefix' => 'api','namespace'=>'Api'], function(){
    Route::get('/theatre/getTheatres', 'TheatreController@getTheatres');
    Route::resource('/theatre', 'TheatreController');
});
//Route::get('/login', 'Auth\LoginController@index');


